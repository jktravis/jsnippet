package com.jktravis.jsnippet;

import com.jktravis.jsnippet.controller.SnippetController;
import com.jktravis.jsnippet.model.Snippet;
import com.jktravis.jsnippet.model.Syntax;
import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

import java.io.IOException;

public class Main extends Application
{
    private Stage primaryStage;
    private BorderPane root;
    private ObservableList<Snippet> snippets = FXCollections.observableArrayList();

    public Main()
    {
        snippets.add(new Snippet("SQL Select", "select * from table;", new Syntax("SQL")));
        snippets.add(new Snippet("Bash For Loop", "for i in {1..10} \n do\n echo \"foo\";\ndone", new Syntax("BASH")));
    }

    public ObservableList<Snippet> getSnippets()
    {
        return snippets;
    }

    @Override
    public void start(Stage primaryStage) throws Exception
    {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("view/RootLayout.fxml"));
        root = (BorderPane) loader.load();
        primaryStage.setTitle("jSnippet");
        primaryStage.setScene(new Scene(root, 800, 600));
        primaryStage.show();

        showSnippetOverview();

    }

    public Stage getPrimaryStage()
    {
        return primaryStage;
    }

    public void showSnippetOverview()
    {
        try
        {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("view/SnippetOverview.fxml"));
            BorderPane overview = (BorderPane) loader.load();
            root.setCenter(overview);

            // Give the controller access to the main app
            SnippetController controller = loader.getController();
            controller.setMainApp(this);
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }
    public static void main(String[] args)
    {
        launch(args);
    }
}
