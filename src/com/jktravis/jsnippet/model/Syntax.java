package com.jktravis.jsnippet.model;

/**
 *
 * @author Joshua Travis
 * @since 2014-08-24
 */
public class Syntax
{
    private String name;

    public Syntax()
    {
    }

    public Syntax(String name)
    {
        this.name = name;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    @Override
    public String toString()
    {
        return getName();
    }
}
