package com.jktravis.jsnippet.model;

import java.util.List;

/**
 * Defines what it means to be a snippet.
 * @author Joshua Travis
 * @since 2014-08-24
 */
public class Snippet
{
    private String name;
    private String body;
    private Syntax syntax;
    private List<Tag> tags;
    private String notes;

    public Snippet()
    {
    }

    public Snippet(String name)
    {
        this.name = name;
    }

    public Snippet(String name, String body)
    {
        this.name = name;
        this.body = body;
    }

    public Snippet(String name, String body, Syntax syntax)
    {
        this.name = name;
        this.body = body;
        this.syntax = syntax;
    }

    public Snippet(String name, String body, Syntax syntax, List<Tag> tags, String notes)
    {
        this.name = name;
        this.body = body;
        this.syntax = syntax;
        this.tags = tags;
        this.notes = notes;
    }

    public List<Tag> getTags()
    {
        return tags;
    }

    public void setTags(List<Tag> tags)
    {
        this.tags = tags;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getBody()
    {
        return body;
    }

    public void setBody(String text)
    {
        this.body = body;
    }

    public Syntax getSyntax()
    {
        return syntax;
    }

    public void setSyntax(Syntax syntax)
    {
        this.syntax = syntax;
    }

    public String getNotes()
    {
        return notes;
    }

    public void setNotes(String notes)
    {
        this.notes = notes;
    }
}
