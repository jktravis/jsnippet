package com.jktravis.jsnippet.model;

/**
 *
 * @author Joshua Travis
 * @since 2014-08-24
 */
public class Tag
{
    private String name;

    public Tag()
    {
    }

    public Tag(String name)
    {
        this.name = name;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }
}
