package com.jktravis.jsnippet.controller;

import com.jktravis.jsnippet.Main;
import com.jktravis.jsnippet.model.Snippet;
import com.jktravis.jsnippet.model.Syntax;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.cell.PropertyValueFactory;


public class SnippetController
{
    private Main mainApp;

    @FXML
    private TableView<Snippet> snippetTable;

    @FXML
    private TableColumn<Snippet, String> snippetNameColumn;

    @FXML
    private TableColumn<Snippet, Syntax> snippetSyntaxColumn;

    @FXML
    private Label snippetNameLabel;

    @FXML
    private TextArea snippetBody;

    public SnippetController()
    {
    }

    @FXML
    private void initialize()
    {
        snippetNameColumn.setCellValueFactory(new PropertyValueFactory<Snippet, String>("name"));
        snippetSyntaxColumn.setCellValueFactory(new PropertyValueFactory<Snippet, Syntax>("syntax"));

        // Auto resize columns
        snippetTable.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);

        // clear person
        showSnippetDetails(null);

        // Listen for selection changes
        snippetTable.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<Snippet>()
        {
            @Override
            public void changed(ObservableValue<? extends Snippet> observableValue, Snippet oldSnippet, Snippet newSnippet)
            {
                saveChanges(oldSnippet);
                showSnippetDetails(newSnippet);
            }
        });
    }

    public void setMainApp(Main mainApp)
    {
        this.mainApp = mainApp;
        snippetTable.setItems(mainApp.getSnippets());
    }

    public void showSnippetDetails(Snippet snippet)
    {
        if (snippet != null)
        {
            snippetBody.setText(snippet.getBody());
        }
        else
        {
            snippetBody.setText("");
        }
    }

    @FXML
    private void handleDeleteSnippet()
    {
        int selectedIndex = snippetTable.getSelectionModel().getSelectedIndex();
        if (selectedIndex >= 0)
            snippetTable.getItems().remove(selectedIndex);
    }

    @FXML
    private void handleNewSnippet()
    {
        Snippet tempSnippet = new Snippet("New Snippet");
        mainApp.getSnippets().add(tempSnippet);
        int index = mainApp.getSnippets().size() - 1;
        snippetTable.getSelectionModel().select(index);
        refreshPersonTable();
        showSnippetDetails(mainApp.getSnippets().get(index));
    }

    @FXML
    private void handleSaveSnippet()
    {
        snippetTable.getSelectionModel().getSelectedItem().setBody(snippetBody.getText());
    }

    /**
     * Refreshes the table. This is only necessary if an item that is already in
     * the table is changed. New and deleted items are refreshed automatically.
     *
     * This is a workaround because otherwise we would need to use property
     * bindings in the model class and add a *property() method for each
     * property. Maybe this will not be necessary in future versions of JavaFX
     * (see http://javafx-jira.kenai.com/browse/RT-22599)
     */
    private void refreshPersonTable()
    {
        int selectedIndex = snippetTable.getSelectionModel().getSelectedIndex();
        snippetTable.setItems(null);
        snippetTable.layout();
        snippetTable.setItems(mainApp.getSnippets());
        // Must set the selected index again (see http://javafx-jira.kenai.com/browse/RT-26291)
        snippetTable.getSelectionModel().select(selectedIndex);
    }

    private void saveChanges(Snippet snippet)
    {
        if (snippet != null)
        {
            for (Snippet s : mainApp.getSnippets())
            {
                if (s.equals(snippet))
                {
                    System.out.println("Snippet: " + snippet.getName() + " is equal to the one passed.");
                }
            }
            snippet.setBody(snippetBody.getText());
        }
        refreshPersonTable();
    }
}
